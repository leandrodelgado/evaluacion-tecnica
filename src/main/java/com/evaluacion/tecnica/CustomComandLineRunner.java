package com.evaluacion.tecnica;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.evaluacion.tecnica.dto.client.ManufacturerClientDto;
import com.evaluacion.tecnica.entity.Manufacturer;
import com.evaluacion.tecnica.entity.VehicleType;
import com.evaluacion.tecnica.entity.VehicleTypeId;
import com.evaluacion.tecnica.repository.ManufacturerRepository;
import com.evaluacion.tecnica.service.ExternalApiService;

@Component
public class CustomComandLineRunner implements CommandLineRunner {

	private final ManufacturerRepository manufacturerRepository;
	private final ExternalApiService externalApiService;
	private final ModelMapper modelMapper;

	public CustomComandLineRunner(@Qualifier("manufacturerRepository") ManufacturerRepository manufacturerRepository,
			@Qualifier("externalApiService") ExternalApiService externalApiService, ModelMapper modelMapper) {
		this.manufacturerRepository = manufacturerRepository;
		this.externalApiService = externalApiService;
		this.modelMapper = modelMapper;
	}

	@Override
	public void run(String... args) throws Exception {
		ManufacturerClientDto result = externalApiService.fetchData();
		if (result != null) {
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

			List<Manufacturer> entities = result.getResults().stream().map(externalManufacturer -> {
				Manufacturer manufacturer = modelMapper.map(externalManufacturer, Manufacturer.class);
				List<VehicleType> vehicleTypes = externalManufacturer.getVehicleTypes().stream()
						.map(externalVehicleType -> {
							VehicleType vehicleType = modelMapper.map(externalVehicleType, VehicleType.class);
							vehicleType
									.setId(new VehicleTypeId(manufacturer.getMfrID(), externalVehicleType.getName()));
							vehicleType.setPrimary(externalVehicleType.getIsPrimary());
							return vehicleType;
						}).collect(Collectors.toList());
				manufacturer.setVehicleTypes(vehicleTypes);
				return manufacturer;
			}).collect(Collectors.toList());

			entities.forEach(entity -> this.manufacturerRepository.save(entity));
		}
	}
}