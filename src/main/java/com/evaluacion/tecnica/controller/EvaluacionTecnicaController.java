package com.evaluacion.tecnica.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.evaluacion.tecnica.service.EvaluacionTecnicaService;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

@RestController
@RequestMapping("/api/evaluacion-tecnica")
public class EvaluacionTecnicaController {

	private static final Logger logger = LoggerFactory.getLogger(EvaluacionTecnicaController.class);

	private EvaluacionTecnicaService evaluacionTecnicaService;

	public EvaluacionTecnicaController(
			@Qualifier("evaluacionTecnicaService") EvaluacionTecnicaService evaluacionTecnicaService) {
		this.evaluacionTecnicaService = evaluacionTecnicaService;
	}

	@GetMapping(value = "/country")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> getByCountry(@RequestParam(value = "format", required = true) String format,
			@RequestParam(value = "country", required = true) String country) {
		try {
			if (format.equals("json")) {
				return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
						.body(this.evaluacionTecnicaService.getByCountry(country));
			} else if (format.equals("xml")) {
				String xmlString = new XmlMapper()
						.writeValueAsString(this.evaluacionTecnicaService.getByCountry(country));
				return ResponseEntity.ok().contentType(MediaType.APPLICATION_XML).body(xmlString);
			} else {
				return ResponseEntity.badRequest().body("Formato no válido. Por favor, utilice 'json' o 'xml'.");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/name")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> getByName(@RequestParam(value = "format", required = true) String format,
			@RequestParam(value = "name", required = true) String name) {
		try {
			if (format.equals("json")) {
				return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON)
						.body(this.evaluacionTecnicaService.getByName(name));
			} else if (format.equals("xml")) {
				String xmlString = new XmlMapper().writeValueAsString(this.evaluacionTecnicaService.getByName(name));
				return ResponseEntity.ok().contentType(MediaType.APPLICATION_XML).body(xmlString);
			} else {
				return ResponseEntity.badRequest().body("Formato no válido. Por favor, utilice 'json' o 'xml'.");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
