package com.evaluacion.tecnica.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ManufacturerDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1888054733697442524L;

	@JsonProperty("Mfr_ID")
	private Long mfrID;

	@JsonProperty("Country")
	private String country;

	@JsonProperty("Mfr_CommonName")
	private String mfrCommonName;

	@JsonProperty("Mfr_Name")
	private String mfrName;

	@JsonProperty("VehicleTypes")
	private List<VehicleTypeDto> vehicleTypes;

	public List<VehicleTypeDto> getVehicleTypes() {
		return vehicleTypes;
	}

	public void setVehicleTypes(List<VehicleTypeDto> vehicleTypes) {
		this.vehicleTypes = vehicleTypes;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Long getMfrID() {
		return mfrID;
	}

	public void setMfrID(Long mfrID) {
		this.mfrID = mfrID;
	}

	public String getMfrCommonName() {
		return mfrCommonName;
	}

	public void setMfrCommonName(String mfrCommonName) {
		this.mfrCommonName = mfrCommonName;
	}

	public String getMfrName() {
		return mfrName;
	}

	public void setMfrName(String mfrName) {
		this.mfrName = mfrName;
	}

}
