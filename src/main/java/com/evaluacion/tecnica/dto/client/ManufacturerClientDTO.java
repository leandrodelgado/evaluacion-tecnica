
package com.evaluacion.tecnica.dto.client;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ManufacturerClientDto {

	@JsonProperty("Results")
	private List<ResultClientDto> results;

	public List<ResultClientDto> getResults() {
		return results;
	}

	public void setResults(List<ResultClientDto> results) {
		this.results = results;
	}

}
