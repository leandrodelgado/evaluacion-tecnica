
package com.evaluacion.tecnica.dto.client;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResultClientDto {

	@JsonProperty("Country")
	private String country;

	@JsonProperty("Mfr_CommonName")
	private String mfrCommonName;

	@JsonProperty("Mfr_ID")
	private Integer mfrID;

	@JsonProperty("Mfr_Name")
	private String mfrName;

	@JsonProperty("VehicleTypes")
	private List<VehicleTypeClientDto> vehicleTypes;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMfrCommonName() {
		return mfrCommonName;
	}

	public void setMfrCommonName(String mfrCommonName) {
		this.mfrCommonName = mfrCommonName;
	}

	public Integer getMfrID() {
		return mfrID;
	}

	public void setMfrID(Integer mfrID) {
		this.mfrID = mfrID;
	}

	public String getMfrName() {
		return mfrName;
	}

	public void setMfrName(String mfrName) {
		this.mfrName = mfrName;
	}

	public List<VehicleTypeClientDto> getVehicleTypes() {
		return vehicleTypes;
	}

	public void setVehicleTypes(List<VehicleTypeClientDto> vehicleTypes) {
		this.vehicleTypes = vehicleTypes;
	}

}
