
package com.evaluacion.tecnica.dto.client;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VehicleTypeClientDto {

	@JsonProperty("IsPrimary")
	private Boolean isPrimary;

	@JsonProperty("Name")
	private String name;

	public Boolean getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
