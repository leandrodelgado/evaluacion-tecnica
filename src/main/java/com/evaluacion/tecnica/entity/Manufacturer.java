package com.evaluacion.tecnica.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "manufacturer")
public class Manufacturer {

	@Id
	private Long mfrID;

	private String country;

	private String mfrCommonName;

	private String mfrName;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private List<VehicleType> vehicleTypes;

	public Long getMfrID() {
		return mfrID;
	}

	public void setMfrID(Long mfrID) {
		this.mfrID = mfrID;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMfrCommonName() {
		return mfrCommonName;
	}

	public void setMfrCommonName(String mfrCommonName) {
		this.mfrCommonName = mfrCommonName;
	}

	public String getMfrName() {
		return mfrName;
	}

	public void setMfrName(String mfrName) {
		this.mfrName = mfrName;
	}

	public List<VehicleType> getVehicleTypes() {
		return vehicleTypes;
	}

	public void setVehicleTypes(List<VehicleType> vehicleTypes) {
		this.vehicleTypes = vehicleTypes;
	}

}
