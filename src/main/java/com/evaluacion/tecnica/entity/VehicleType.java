package com.evaluacion.tecnica.entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "vehicletype")
public class VehicleType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1882952336247339263L;

	@EmbeddedId
	private VehicleTypeId id;

	private boolean isPrimary;

	public VehicleTypeId getId() {
		return id;
	}

	public void setId(VehicleTypeId id) {
		this.id = id;
	}

	public boolean isPrimary() {
		return isPrimary;
	}

	public void setPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

}
