package com.evaluacion.tecnica.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class VehicleTypeId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 387591780701642513L;

	private Long mfrID;

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getMfrID() {
		return mfrID;
	}

	public void setMfrID(Long mfrID) {
		this.mfrID = mfrID;
	}

	public VehicleTypeId(Long mfrID, String name) {
		super();
		this.mfrID = mfrID;
		this.name = name;
	}
	
	public VehicleTypeId() {}

}