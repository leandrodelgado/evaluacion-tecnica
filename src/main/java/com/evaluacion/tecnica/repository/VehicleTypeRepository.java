package com.evaluacion.tecnica.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.evaluacion.tecnica.entity.Manufacturer;

@Repository("vehicleTypeRepository")
public interface VehicleTypeRepository extends JpaRepository<Manufacturer, Long> {

	public List<Manufacturer> findByCountry(String country);

}
