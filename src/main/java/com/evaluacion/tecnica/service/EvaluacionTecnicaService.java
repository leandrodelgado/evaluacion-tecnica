package com.evaluacion.tecnica.service;

import java.util.List;

import com.evaluacion.tecnica.dto.ManufacturerDto;

public interface EvaluacionTecnicaService {

	List<ManufacturerDto> getByCountry(String country);

	List<ManufacturerDto> getByName(String name);

}
