package com.evaluacion.tecnica.service;

import com.evaluacion.tecnica.dto.client.ManufacturerClientDto;

public interface ExternalApiService {

	ManufacturerClientDto fetchData();

}
