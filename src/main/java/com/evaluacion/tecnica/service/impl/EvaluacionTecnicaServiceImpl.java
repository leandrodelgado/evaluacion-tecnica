package com.evaluacion.tecnica.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.evaluacion.tecnica.dto.ManufacturerDto;
import com.evaluacion.tecnica.dto.VehicleTypeDto;
import com.evaluacion.tecnica.entity.Manufacturer;
import com.evaluacion.tecnica.repository.ManufacturerRepository;
import com.evaluacion.tecnica.service.EvaluacionTecnicaService;

@Service("evaluacionTecnicaService")
public class EvaluacionTecnicaServiceImpl implements EvaluacionTecnicaService {

	private ManufacturerRepository manufacturerRepository;

	private final ModelMapper modelMapper;

	public EvaluacionTecnicaServiceImpl(
			@Qualifier("manufacturerRepository") ManufacturerRepository manufacturerRepository,
			ModelMapper modelMapper) {
		this.manufacturerRepository = manufacturerRepository;
		this.modelMapper = modelMapper;
	}

	/**
	 * Metodo encargado de invocar a JPA para obtener los registros cuyo country sea
	 * igual al parametro
	 * 
	 * @param country El pais utilizado como filtro
	 */
	@Override
	@Cacheable("countryCache")
	public List<ManufacturerDto> getByCountry(String country) {
		return this.mapperResult(this.manufacturerRepository.findByCountry(country));
	}

	/**
	 * Metodo encargado de invocar a JPA para obtener los registros cuyo campo
	 * MfrName sea igual al parametro
	 * 
	 * @param name El nombre utilizado como filtro
	 */
	@Override
	@Cacheable("nameCache")
	public List<ManufacturerDto> getByName(String name) {
		return this.mapperResult(this.manufacturerRepository.findByMfrName(name));
	}

	/**
	 * Metodo que realiza el mapeo de una lista de entidades a una lista de
	 * elementos DTO
	 * 
	 * @param manufacturers Lista de entidades
	 * @return
	 */
	private List<ManufacturerDto> mapperResult(List<Manufacturer> manufacturers) {
		return manufacturers.stream().map(manufacturer -> {
			ManufacturerDto manufacturerDTO = modelMapper.map(manufacturer, ManufacturerDto.class);
			List<VehicleTypeDto> vehicleTypeDTOs = manufacturer.getVehicleTypes().stream().map(vehicleType -> {
				VehicleTypeDto vehicleTypeDto = modelMapper.map(vehicleType, VehicleTypeDto.class);
				vehicleTypeDto.setName(vehicleType.getId().getName());
				vehicleTypeDto.setIsPrimary(vehicleType.isPrimary());
				return vehicleTypeDto;
			}).collect(Collectors.toList());
			manufacturerDTO.setVehicleTypes(vehicleTypeDTOs);
			return manufacturerDTO;
		}).collect(Collectors.toList());
	}

}
