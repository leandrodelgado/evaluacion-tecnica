package com.evaluacion.tecnica.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.evaluacion.tecnica.dto.client.ManufacturerClientDto;
import com.evaluacion.tecnica.service.ExternalApiService;

@Service("externalApiService")
public class ExternalApiServiceImpl implements ExternalApiService {

	@Value("${external.api.url}")
	private String externalApiUrl;

	private final RestTemplate restTemplate;

	public ExternalApiServiceImpl(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Override
	public ManufacturerClientDto fetchData() {
		return restTemplate.getForObject(externalApiUrl, ManufacturerClientDto.class);
	}

}
