package com.evaluacion.tecnica.controller;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.evaluacion.tecnica.dto.ManufacturerDto;
import com.evaluacion.tecnica.mocks.ManufacturerMockResponse;
import com.evaluacion.tecnica.service.EvaluacionTecnicaService;

@WebMvcTest(EvaluacionTecnicaControllerTest.class)
public class EvaluacionTecnicaControllerTest {

	@Mock
	private EvaluacionTecnicaService evaluacionTecnicaService;

	@InjectMocks
	private EvaluacionTecnicaController evaluacionTecnicaController;

	@Test
	public void testGetByCountry() {
		String country = "UNITED STATES (USA)";
		List<ManufacturerDto> datosTest = ManufacturerMockResponse.getDatosTest();
		when(evaluacionTecnicaService.getByCountry(country)).thenReturn(datosTest);
		ResponseEntity<?> response = evaluacionTecnicaController.getByCountry("json", country);

		verify(evaluacionTecnicaService, times(1)).getByCountry(country);

		assertEquals(HttpStatus.OK, response.getStatusCode());

		assertEquals(datosTest, response.getBody());
	}

	@Test
	public void testGetByCountryNotFound() {
		String country = "USA";
		when(evaluacionTecnicaService.getByCountry(country)).thenReturn(null);
		ResponseEntity<?> response = evaluacionTecnicaController.getByCountry("json", country);

		verify(evaluacionTecnicaService, times(1)).getByCountry(country);

		assertEquals(HttpStatus.OK, response.getStatusCode());

		assertNull(response.getBody());
	}

	@Test
	public void testGetByName() {
		String name = "TESLA, INC.";
		List<ManufacturerDto> datosTest = ManufacturerMockResponse.getDatosTest();
		when(evaluacionTecnicaService.getByName(name)).thenReturn(datosTest);
		ResponseEntity<?> response = evaluacionTecnicaController.getByName("json", name);

		verify(evaluacionTecnicaService, times(1)).getByName(name);

		assertEquals(HttpStatus.OK, response.getStatusCode());

		assertEquals(datosTest, response.getBody());
	}

	@Test
	public void testGetByNameNotFound() {
		String name = "TESLA";
		when(evaluacionTecnicaService.getByName(name)).thenReturn(null);
		ResponseEntity<?> response = evaluacionTecnicaController.getByName("json", name);

		verify(evaluacionTecnicaService, times(1)).getByName(name);

		assertEquals(HttpStatus.OK, response.getStatusCode());

		assertNull(response.getBody());
	}

}
