package com.evaluacion.tecnica.mocks;

import java.util.ArrayList;
import java.util.List;

import com.evaluacion.tecnica.dto.ManufacturerDto;
import com.evaluacion.tecnica.dto.VehicleTypeDto;

public class ManufacturerMockResponse {

	public static List<ManufacturerDto> getDatosTest() {

		List<ManufacturerDto> manufacturerDtos = new ArrayList<>();

		ManufacturerDto mockManufacturer = new ManufacturerDto();
		mockManufacturer.setCountry("UNITED STATES (USA)");
		mockManufacturer.setMfrCommonName("Tesla");
		mockManufacturer.setMfrID(955L);
		mockManufacturer.setMfrName("TESLA, INC.");

		List<VehicleTypeDto> vehicleTypes = new ArrayList<>();
		vehicleTypes.add(new VehicleTypeDto(true, "Passenger Car"));
		vehicleTypes.add(new VehicleTypeDto(false, "Truck"));
		vehicleTypes.add(new VehicleTypeDto(false, "Multipurpose Passenger Vehicle (MPV)"));

		manufacturerDtos.add(mockManufacturer);
		return manufacturerDtos;
	}

}
